import React, { Fragment } from "react";
import './App.scss'
import Header from "./pages/common-components/header/Header";
import Footer from "./pages/common-components/footer/Footer";
import Home from "./pages/home/Home";
import Blog from "./pages/blog/Blog";

const App = () => {
  return (
    <Fragment>
      <Header />
      <div className="content-wrap">
        <Home />
      </div>
    </Fragment>
  );
};

export default App;
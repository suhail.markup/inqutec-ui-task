import React, { Fragment } from 'react';
import styled from 'styled-components';

const HomeWrapper = styled.div`
`;

const Home = () => {
  return (
    <Fragment>
      {/* home page starts */}
      <HomeWrapper>
      </HomeWrapper>
      {/* home page ends */}
    </Fragment>
  );
};

export default Home;

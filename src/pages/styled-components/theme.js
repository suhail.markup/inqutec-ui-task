export const theme = {
  // FONT-WEIGHTS
  regular: 400, // default

  // FONT-SIZES
  mini: '13px',
  base: '14px', // default font size
  medium: '15px',



  // LINE-HEIGHTS
  lineHeight1_41: 1.45, // default

  // TEXT-TRANSFORMS
  textUppercase: 'uppercase',
  textLowercase: 'lowercase',

  // COLORS
  baseColor: '#19181D', // default
  white: '#FFFFFF',

  // BACKGROUND COLORS
  brandPrimary: '#E5E5E5',
  brandSecondary: '#F4F4F4',

  // Z-INDEX
  zIndex1: 1,
  zIndex2: 2,
  zIndex3: 3,
  zIndex4: 4,
  zIndex5: 5,
  zIndex6: 6,
  zIndex7: 7,
  zIndex8: 8,
  zIndex9: 9,
  zIndex10: 10,

  // VIEWPORT HEIGHT
  vhHeight: '100vh'
};
